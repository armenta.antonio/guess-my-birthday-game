from random import randint

turns     = 5
months    = [
    "",
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
]


guess_num = 1     # init guess_num variable and
try_again = False # set try_again to False
user_name = input("Hi! What is your name? ")

while (guess_num <= turns):
    if (try_again == False):
        month_int = randint(1,12)
        year_int = randint(1924,2004)
    try_again = False
    print("Guess", guess_num, ":",user_name, "were you born in",months[month_int],"of",year_int,"?")
    true_or_false = input("Please type yes or no: ")
    if (true_or_false == "yes"):
        print("I knew it!")
        break
    elif (true_or_false == "no" and guess_num <= turns-1):
        print("Drat! Lemme try again!")
    elif (true_or_false == "no" and guess_num >= turns):
        print("I have other things to do. Good bye.")
    else:
        print("invalid input. Please try again")
        # this section causes the loop to, in essence, repeat the last 
        # guess instead of booting the user out of the program
        guess_num = guess_num - 1
        try_again = True
    guess_num = guess_num + 1


    